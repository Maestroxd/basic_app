const reasonInput = document.querySelector('#input-reason');
const amountInput = document.querySelector('#input-amount');
const cancelBtn = document.querySelector('#btn-cancel');
const confirmBtn = document.querySelector('#btn-confirm');
const expensesList = document.querySelector('#expenses-list');
const totalExpensesOutput = document.querySelector('#total-expenses');
const alertCtrl = document.querySelector('ion-alert-controller');

let totalExpenses = 0;

const clear = () => {
    reasonInput.value='';
    amountInput.value='';
}

confirmBtn.addEventListener('click',() => {
    const enteredReason = reasonInput.value;
    const enteredAmount = amountInput.value;

    if(enteredReason.trim().length<=0||enteredAmount<=0||enteredAmount.trim()<=0){
       alertCtrl.create({message: 'Please enter a valid reason and amount!', header: 'Invalid inputs',
       buttons : ['Okay']}) .then(alertElement => {
           alertElement.present();
       })
    return;
    }
    const newItem = document.createElement('ion-item');
    

    expensesList.appendChild(newItem);

    totalExpenses += +enteredAmount;
    totalExpensesOutput.textContent = totalExpenses;

    const percentage = enteredAmount/totalExpenses*100;


    if(percentage >= 50 && percentage < 100 || enteredAmount > 100 ){
            alertCtrl.create({message: 'Be careful, your last expense was very high!', header: 'High expense',
            buttons : ['Okay']}) .then(alertElement => {
                alertElement.present();
            })
    }
    newItem.textContent = enteredReason + ': $'+ enteredAmount;

    clear();
});

cancelBtn.addEventListener('click',clear);
